package application;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ConfigLoader {


	private Properties prop;
	private InputStream in;

	private Map<String, Long> conversionsRate;

	public ConfigLoader(){

		conversionsRate = new HashMap<String, Long>();

		loadPropertiesFile();
		initializeConversionRates();
	}

	private void loadPropertiesFile() {
		try {
			in = this.getClass().getClassLoader().getResourceAsStream("config.properties");
			prop = new Properties();
			prop.load(in);
		} catch (IOException e) {
			System.err.println("LULZ U FAILED");
			e.printStackTrace();
		}
	}

	private void initializeConversionRates(){
		Enumeration<?> properties = prop.keys();
		while (properties.hasMoreElements()) {
			try {
				String key = (String) properties.nextElement();
				String value = prop.getProperty(key);
				long conversionToByte = Long.parseLong(value);
				conversionsRate.put(key, conversionToByte);
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR, "Invalid config.properties file!");
				alert.showAndWait();
				System.exit(0);
			}
		}
	}
	
	public Map<String, Long> getConversionRates(){
		return conversionsRate;
	}
}
