package application.about;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AboutWindowController implements Initializable{
	
	private URI twitter;
	private URI bitbucket;
	private URI gitHub;
	private URI byCC;
	
	@FXML
	private ImageView elegeaLogo;
	
	@FXML
	private ImageView iconTwitter;
	
	@FXML
	private ImageView iconBitbucket;
	
	@FXML
	private ImageView iconGitHub;
	
	@FXML
	private ImageView iconCCBy;
	
	@FXML
	private Hyperlink hyperlinkTwitter;
	
	@FXML
	private Hyperlink hyperlinkBitbucket;
	
	@FXML
	private Hyperlink hyperlinkGitHub;
	
	@FXML
	private Hyperlink hyperlinkCCBy;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setupLinks();
		setupButtons();
		setupImages();
	}
	
	private void setupLinks(){
		try{
			twitter = new URI("https://twitter.com/elegeadotcom");
			bitbucket = new URI("https://bitbucket.org/jm_duarte/");
			gitHub = new URI("https://github.com/jm-duarte");
			byCC = new URI("https://creativecommons.org/licenses/by/4.0/");
		}  catch (URISyntaxException e1) {
			System.out.println("Something went wrong :/");
			e1.printStackTrace();
		}
	}
	
	private void setupButtons(){
		hyperlinkTwitter.setOnAction(e -> openLink(twitter));
		hyperlinkBitbucket.setOnAction(e -> openLink(bitbucket));
		hyperlinkGitHub.setOnAction(e -> openLink(gitHub));
		hyperlinkCCBy.setOnAction(e -> openLink(byCC));
	}
	
	private void openLink(URI link){
		try {
			Desktop.getDesktop().browse(link);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setupImages(){
		Image elegeaImg = new Image(AboutWindowController.class.getResourceAsStream("/resources/ELEGEA_steam.jpg"));
		Image twitterImg = new Image(AboutWindowController.class.getResourceAsStream("/resources/twitter.png"));
		Image bitbucketImg = new Image(AboutWindowController.class.getResourceAsStream("/resources/bitbucket.png"));
		Image gitHubImg = new Image(AboutWindowController.class.getResourceAsStream("/resources/github.png"));
		Image ccByImg = new Image(AboutWindowController.class.getResourceAsStream("/resources/ccby.png"));
		elegeaLogo.setImage(elegeaImg);
		iconTwitter.setImage(twitterImg);
		iconBitbucket.setImage(bitbucketImg);
		iconGitHub.setImage(gitHubImg);
		iconCCBy.setImage(ccByImg);
	}
}
