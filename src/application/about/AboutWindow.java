package application.about;

import java.io.IOException;

import application.main.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AboutWindow {
	public static void display() {
		Parent root;
		try {
			root = FXMLLoader.load(AboutWindow.class.getResource("AboutWindow.fxml"));

			Stage aboutWindow = new Stage();

			aboutWindow.initModality(Modality.APPLICATION_MODAL);
			aboutWindow.setTitle("About");
			aboutWindow.getIcons().add(new Image(Main.class.getResourceAsStream("/resources/ftransfer_logo.png")));
			Scene scene = new Scene(root);
			aboutWindow.setScene(scene);
			aboutWindow.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
