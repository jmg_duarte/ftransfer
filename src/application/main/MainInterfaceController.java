package application.main;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import application.ConfigLoader;
import application.about.AboutWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class MainInterfaceController implements Initializable {

	private static final String ERROR_NOT_VALID_INPUT = "The %s/%s must be a number!";
	private static final String FILE_SIZE = "file size";
	private static final String TRANSFER_SPEED = "transfer speed";
	private static final String TRANSFER_TIME = "transfer time";

	/*
	 * Declarations of the interface interactive bits FS - File Size TS -
	 * Transfer Speed TT - Transfer Time
	 */
	@FXML
	private TextField FSTextField;

	@FXML
	private ChoiceBox<String> FSChoiceBox;

	@FXML
	private RadioButton FSRadioButton;

	@FXML
	private TextField TSTextField;

	@FXML
	private ChoiceBox<String> TSChoiceBox;

	@FXML
	private RadioButton TSRadioButton;

	@FXML
	private TextField TTTextField;

	@FXML
	private ChoiceBox<String> TTChoiceBox;

	@FXML
	private RadioButton TTRadioButton;

	@FXML
	private Button aboutButton;

	@FXML
	private Button calculateButton;

	// Data Sets
	private ObservableList<String> fileSizes = FXCollections.observableArrayList("KB", "MB", "GB", "TB", "PB");
	private ObservableList<String> transferSpeeds = FXCollections.observableArrayList("KB/s", "MB/s", "GB/s", "TB/s",
			"PB/s");
	private ObservableList<String> timeUnits = FXCollections.observableArrayList("Seconds", "Minutes", "Hours");

	private Map<String, Long> conversions;
	private ConfigLoader config;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		config = new ConfigLoader();

		conversions = config.getConversionRates();

		setupApplication();
	}

	private void setupApplication(){
		populateUISetDefaults();
		setRadioButtonsActions();
		setButtonsActions();
		setListeners();
	}
	
	private void populateUISetDefaults() {
		FSChoiceBox.setItems(fileSizes);
		FSChoiceBox.setValue(fileSizes.get(1));

		TSChoiceBox.setItems(transferSpeeds);
		TSChoiceBox.setValue(transferSpeeds.get(1));
		TSTextField.setText("50");

		TTChoiceBox.setItems(timeUnits);
		TTChoiceBox.setValue(timeUnits.get(0));
		TTTextField.setText("10");
	}

	// Radio Buttons
	private void setRadioButtonsActions() {
		FSRadioButton.setOnAction(e -> clearRadioButtons(e));
		TSRadioButton.setOnAction(e -> clearRadioButtons(e));
		TTRadioButton.setOnAction(e -> clearRadioButtons(e));
	}

	private void clearRadioButtons(ActionEvent e) {
		if (e.getSource() == FSRadioButton) {
			FSRadioButton.setSelected(true);
			TSRadioButton.setSelected(false);
			TTRadioButton.setSelected(false);
		}

		if (e.getSource() == TSRadioButton) {
			FSRadioButton.setSelected(false);
			TSRadioButton.setSelected(true);
			TTRadioButton.setSelected(false);
		}

		if (e.getSource() == TTRadioButton) {
			FSRadioButton.setSelected(false);
			TSRadioButton.setSelected(false);
			TTRadioButton.setSelected(true);
		}
	}

	private void setButtonsActions(){
		calculateButton.setOnAction(e -> calculateValues());
		aboutButton.setOnAction(e -> AboutWindow.display());
	}
	
	// Calculations
	private void calculateValues() {
		if (FSRadioButton.isSelected()) {
			try {
				String TSText = TSTextField.getText();
				double transferSpeed = Double.parseDouble(TSText);

				String TTText = TTTextField.getText();
				double transferTime = Double.parseDouble(TTText);

				FSTextField.setText(Double.toString(calculateFileSize(transferSpeed, transferTime)));
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR,
						String.format(ERROR_NOT_VALID_INPUT, TRANSFER_SPEED, TRANSFER_TIME));
				alert.showAndWait();
			}
		}

		if (TSRadioButton.isSelected()) {
			try {
				String FSText = FSTextField.getText();
				double fileSize = Double.parseDouble(FSText);

				String TTText = TTTextField.getText();
				double transferTime = Double.parseDouble(TTText);

				TSTextField.setText(Double.toString(calculateTransferSpeed(fileSize, transferTime)));
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR,
						String.format(ERROR_NOT_VALID_INPUT, FILE_SIZE, TRANSFER_TIME));
				alert.showAndWait();
			}
		}

		if (TTRadioButton.isSelected()) {
			try {
				String FSText = FSTextField.getText();
				double fileSize = Double.parseDouble(FSText);

				String TSText = TSTextField.getText();
				double transferSpeed = Double.parseDouble(TSText);

				TTTextField.setText(Double.toString(calculateTransferTime(fileSize, transferSpeed)));
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR,
						String.format(ERROR_NOT_VALID_INPUT, FILE_SIZE, TRANSFER_SPEED));
				alert.showAndWait();
			}
		}
	}

	private double calculateFileSize(double transferSpeed, double transferTime) {
		return ((transferSpeed * conversions.get(TSChoiceBox.getValue()) / conversions.get(FSChoiceBox.getValue())
				* transferTime) * conversions.get(TTChoiceBox.getValue()));
	}

	private double calculateTransferSpeed(double fileSize, double transferTime) {
		return ((fileSize / transferTime) * conversions.get(FSChoiceBox.getValue()))
				/ conversions.get(TSChoiceBox.getValue());
	}

	private double calculateTransferTime(double fileSize, double transferSpeed) {
		return ((fileSize * conversions.get(FSChoiceBox.getValue()))
				/ (transferSpeed * conversions.get(TSChoiceBox.getValue())) * conversions.get(TTChoiceBox.getValue()));
	}

	private void setListeners(){
		FSChoiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> FSTextField.setText(converter(FSTextField.getText(), oldValue, newValue)));
		TSChoiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> TSTextField.setText(converter(TSTextField.getText(), oldValue, newValue)));
		TTChoiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> TTTextField.setText(converter(TTTextField.getText(), oldValue, newValue)));
	}
	
	private String converter(String oldValue, String oldUnit ,String newUnit){
		try {
			String newValue = "";
			double value = Double.parseDouble(oldValue);
			value = (value * conversions.get(oldUnit)) / conversions.get(newUnit);
			newValue = Double.toString(value);
			return newValue;
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR, "The input value must be a number!");
			alert.showAndWait();
			return oldValue;
		}
	}
}
